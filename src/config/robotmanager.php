<?php

return [
	//unique name from the docker container	
	'container'	=> 'robotframework',
	//defines the allowed file extension
	'extension'	=> 'robot',
	//where to store the log files
	'log_path'	=> '/robot/logs',
	//where are the testcases located
	'case_path'	=> '/robot/testcases'
];