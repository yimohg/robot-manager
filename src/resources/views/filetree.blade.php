@extends('dockermanager::layouts.master')

@section('content')

	<nav>
  		<ul class="pager">
    		<li class="previous"><a href="{{ route('robot.testing')}}"><span aria-hidden="true">&larr;</span> Test Cases</a></li>
  		</ul>
	</nav>

<div class="row">
	<div class="col-md-12">
		<div id="menu">
			<div class="panel list-group">
				@foreach($lists as $list => $files)
					<a href="#" class="list-group-item" data-toggle="collapse" data-target="#{{$list}}" data-parent="#menu">
						{{ date('d.m.y H:i:s', $list)}} 

						<?php 
							try {
								$outputfile = $files->filter(function ($value, $key) {
								    return $value->getFilename() == "output.xml";
								}); 

								$outputxml = \XmlParser::load($outputfile->first()->getPath().'/output.xml');

								$stats = $outputxml->parse([
								    'success' => ['uses' => 'statistics.suite.stat::pass'],
								    'failed' => ['uses' => 'statistics.suite.stat::fail']
								]); 

								$success 	= $stats['success'];
								$failed 	= $stats['failed'];
							} catch( \Exception $e) {
								debug($e);
								$success 	= 0;
								$failed 	= 0;
							}
							
						?>
						<span class="label label-success pull-right">Passed: {{ $success }}</span> &nbsp;&nbsp;&nbsp;
						<span class="label label-danger pull-right">Failed: {{ $failed }}</span> 
					</a>
					<div id="{{$list}}" class="sublinks collapse">
						@foreach($files as $file)
						
							@if( $file->getExtension() == "html" ) 


								<a 
								class="list-group-item small" 
								href="{{ route('robot.showdetails', ['file' => base64_encode($file->getRealPath()) ])}}" 
								target="_blank">
									<span class="glyphicon glyphicon-link"></span> 
									{{$file->getFilename()}}
								</a>
								
							@else 
								<a class="list-group-item small"><span class="glyphicon glyphicon-chevron-right"></span> {{$file->getFilename()}}</a>
							@endif

						@endforeach
					</div>
				@endforeach
				
			</div>
		</div>
	</div>
</div>




@endsection