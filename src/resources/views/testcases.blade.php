@extends('dockermanager::layouts.master')

@section('content')

	<nav>
  		<ul class="pager">
    		<li class="previous"><a href="{{ url('/')}}"><span aria-hidden="true">&larr;</span> Overview</a></li>
  		</ul>
	</nav>

	<div class="table-responsive">
		<table class="table">
			<thead> 
				<tr>
					<th class="col-xs-1">#</th> 
					<th class="col-xs-10">Test Case</th> 
					<th class="col-xs-1"></th> 
				</tr> 
			</thead> 
			<tbody> 
				@foreach ($testcases as $key => $testcase)
				<tr> 
					<th scope="row">{{ $key }}</th>  
					<td>
						{{ $testcase->getFilename() }}
					</td>
					<td>
						
						<div class="btn-group">
					  		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    		<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
					  		</button>
					  		<ul class="dropdown-menu pull-right">
							    
								<li>
									<a href="{{ route('robot.testcase', ['filename' => $testcase->getBasename('.robot')])}}">Run Testcase
									</a>
								</li>
								<li>
									<a href="{{ route('robot.showlog', ['filename' => $testcase->getBasename('.robot')])}}">Show Logs
									</a>
								</li>
						    	

					  		</ul>
						</div>

					</td> 
				</tr> 
				@endforeach
			</tbody> 
		</table>
	</div>
@endsection