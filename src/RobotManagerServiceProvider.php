<?php

namespace yimOHG\RobotManager;

use Illuminate\Support\ServiceProvider;

class RobotManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Http/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/resources/views', 'robotmanager');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/robotmanager'),
        ]);

        $this->publishes([
            __DIR__.'/config/robotmanager.php' => config_path('robotmanager.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/robotmanager.php', 'robotmanager'
        );
    }
}
