<?php 

Route::get('testing', [
    'as' 	=> 'robot.testing', 
    'uses' 	=> 'yimOHG\RobotManager\Http\Controllers\RobotController@showTestcases'
]);

Route::get('testing/run/{filename}', [
    'as' 	=> 'robot.testcase', 
    'uses' 	=> 'yimOHG\RobotManager\Http\Controllers\RobotController@executeTest'
]);

Route::get('testing/log/{filename}', [
    'as' 	=> 'robot.showlog', 
    'uses' 	=> 'yimOHG\RobotManager\Http\Controllers\RobotController@showLog'
]);

Route::get('testing/log/details/{file}', [
    'as' 	=> 'robot.showdetails', 
    'uses' 	=> 'yimOHG\RobotManager\Http\Controllers\RobotController@showdetails'
]);